import React, { Component } from 'react';
import './App.css';
import { Router, Switch, Route} from 'react-router-dom';
import  { Login } from './login/';
import { SignUp } from './signup/';
import { Home } from './home/';
import { Form } from './form/';
import { history } from './_helpers';
import { PrivateRoute } from './_components';


class App extends Component {
    render() {
       return (
           <div className="App">
               <Router history={history}>
                 <div>
                     <Switch>
                        <Route exact path='/' component={Login} />
                        <Route exact path='/sign-up' component={SignUp} />
                        <PrivateRoute exact path='/home' component={Home} />
                        <PrivateRoute exact path='/send-money' component={Form} />
                     </Switch>
                 </div>
               </Router>
           </div>
        );
     }
}export default App;