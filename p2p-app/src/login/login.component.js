import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
// import { withRouter } from 'react-router-dom';
import { Link as RouterLink, withRouter } from 'react-router-dom';


import { userActions } from '../_actions';
import { history } from '../_helpers';
import {
    Button,
    Grid,
    TextField,
    Typography,
    IconButton,
    Link
   

} from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';


// const schema = {
//     email: {
//         presence: { allowEmpty: false, message: 'is required' },
//         email: true,
//         length: {
//             maximum: 64
//         }
//     },
//     password: {
//         presence: { allowEmpty: false, message: 'is required' },
//         length: {
//             maximum: 128
//         }
//     }
// };
const styles = theme => ({
    root: {
        backgroundColor: theme.palette.background.default,
        height: '100%'
    },
    grid: {
        height: '100%'
    },
    quoteContainer: {
        [theme.breakpoints.down('md')]: {
            display: 'none'
        }
    },
    quote: {
        backgroundColor: theme.palette.neutral,
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundImage: 'url(/images/auth.jpg)',
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center'
    },
    quoteInner: {
        textAlign: 'center',
        flexBasis: '600px'
    },
    quoteText: {
        color: theme.palette.white,
        fontWeight: 300
    },
    name: {
        marginTop: theme.spacing(3),
        color: theme.palette.white
    },
    bio: {
        color: theme.palette.white
    },
    contentContainer: {},
    content: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column'
    },
    contentHeader: {
        display: 'flex',
        alignItems: 'center',
        paddingTop: theme.spacing(5),
        paddingBototm: theme.spacing(2),
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(2)
    },
    logoImage: {
        marginLeft: theme.spacing(4)
    },
    contentBody: {
        flexGrow: 1,
        display: 'flex',
        alignItems: 'center',
        [theme.breakpoints.down('md')]: {
            justifyContent: 'center'
        }
    },
    form: {
        paddingLeft: 100,
        paddingRight: 100,
        paddingBottom: 125,
        flexBasis: 700,
        [theme.breakpoints.down('sm')]: {
            paddingLeft: theme.spacing(2),
            paddingRight: theme.spacing(2)
        }
    },
    title: {
        marginTop: theme.spacing(3)
    },
    socialButtons: {
        marginTop: theme.spacing(3)
    },
    socialIcon: {
        marginRight: theme.spacing(1)
    },
    sugestion: {
        marginTop: theme.spacing(2)
    },
    textField: {
        marginTop: theme.spacing(2)
    },
    signInButton: {
        margin: theme.spacing(2, 0)
    },
});


class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: '',
            showPassword: false,
        }
    }
    componentDidMount() {
        if (localStorage.getItem('user')) {
            history.push('./home')
        }
    }
    handleChange = prop => event => {
        this.setState({ [prop]: event.target.value })
    }
    login = event => {
        this.setState({ suubmitted: true });
        const { username, password } = this.state;
        const { dispatch } = this.props;
        if (username && password) {
            dispatch(userActions.login(username, password))
        }
    }
    render() {
        const { classes } = this.props;
        return (
            <div className={classes.root}>
                <Grid
                    className={classes.grid}
                    container
                >
                    <Grid
                        className={classes.quoteContainer}
                        item
                        lg={5}
                    >
                        <div className={classes.quote}>
                            <div className={classes.quoteInner}>
                                <Typography
                                    className={classes.quoteText}
                                    variant="h1"
                                >
                                    peer to peer payment
                                </Typography>
                                <div className={classes.person}>
                                    <Typography
                                        className={classes.name}
                                        variant="body1"
                                    >

                                    </Typography>
                                    <Typography
                                        className={classes.bio}
                                        variant="body2"
                                    >
                                    </Typography>
                                </div>
                            </div>
                        </div>
                    </Grid>
                    <Grid
                        className={classes.content}
                        item
                        lg={7}
                        xs={12}
                    >
                        <div className={classes.content}>
                            <div className={classes.contentHeader}>
                                <IconButton >
                                    <ArrowBackIcon />
                                </IconButton>
                            </div>
                            <div className={classes.contentBody}>
                                <div
                                    className={classes.form}

                                >
                                    <Typography
                                        className={classes.title}
                                        variant="h2"
                                    >
                                        Sign in
                                    </Typography>
                                   

                                    <Typography
                                        align="center"
                                        className={classes.sugestion}
                                        color="textSecondary"
                                        variant="body1"
                                    >
                                        login with email address
                                    </Typography>
                                    <TextField
                                        className={classes.textField}
                                        fullWidth

                                        label="Email address"
                                        value={this.state.username}
                                        onChange={this.handleChange('username')}
                                        type="text"                                        
                                        variant="outlined"
                                    />
                                    <TextField
                                        className={classes.textField}
                                        fullWidth

                                        label="Password"
                                        value={this.state.password}
                                        onChange={this.handleChange('password')}
                                        type="password"
                                        variant="outlined"
                                    />
                                    <Button
                                        className={classes.signInButton}
                                        color="primary"
                                        fullWidth
                                        size="large"
                                        type="submit"
                                        variant="contained"
                                        onClick={(event)=>{this.login()}}
                                    >
                                        Sign in now
                                    </Button>
                                    <Typography
                                        color="textSecondary"
                                        variant="body1"
                                    >
                                        Don't have an account?{' '}
                                        <Link
                                            component={RouterLink}
                                            to="/sign-up"
                                            variant="h6"
                                        >
                                            Sign up
                  </Link>
                                    </Typography>
                                </div>
                            </div>
                        </div>
                    </Grid>
                </Grid>
            </div>
        )
    }
}

Login.propTypes = {
    classes: PropTypes.object.isRequired
};


const mapStateToProps = (state) => {
    const { loggingIn } = state.authentication;
    return {
       loggingIn
    };
}
const connectedLoginPage = withRouter(connect(mapStateToProps, null, null, {
    pure: false
})(withStyles(styles)(Login)));


export { connectedLoginPage as Login };