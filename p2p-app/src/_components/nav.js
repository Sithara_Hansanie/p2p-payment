import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import HomeIcon from '@material-ui/icons/Home';
import LogoutIcon from '@material-ui/icons/HighlightOff';
import VendorIcon from '@material-ui/icons/CardTravel';
import {
    List,
    Drawer,
    ListItem,
    Divider,
    ListItemIcon,
    ListItemText,
    Typography,
    Avatar
} from '@material-ui/core';

import { userActions } from '../_actions';
import { connect } from 'react-redux';

const drawerWidth = 240;

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    appFrame: {
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
        width: '100%',
    },
    appBar: {
        width: `calc(100% - ${drawerWidth}px)`,
    },
    'appBar-left': {
        marginLeft: drawerWidth,
    },
    'appBar-right': {
        marginRight: drawerWidth,
    },
    drawerPaper: {
        position: 'relative',
        width: drawerWidth,
    },
    toolbar: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing(3),
    },
    item: {
        display: 'flex',
        paddingTop: 0,
        paddingBottom: 0
    },
    avatar: {
        width: 60,
        height: 60
    },
    avatarimg: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        minHeight: 'fit-content',
        padding: 20
    }
});


class Navigation extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            anchor: 'left',
        }
    }

    logout = event => {
        const { dispatch } = this.props;
        console.log(this.props);
        console.log(localStorage.getItem('user'));
        dispatch(userActions.logout());
    }

    render() {
        const { classes } = this.props;
        const { anchor } = this.state;

        return (
            <Drawer
                variant="permanent"
                classes={{
                    paper: classes.drawerPaper,
                }}
                anchor={anchor}
            >
                <div className={classes.avatarimg}>
                    <Avatar
                        alt="Person"
                        className={classes.avatar}
                        // component={RouterLink}
                        // src={user.avatar}
                        to="/settings"
                    />
                    <Typography
                        className={classes.name}
                        variant="h4"
                    >
                        Jim
                    </Typography>
                </div>

                <List component="nav">
                    <ListItem button component='a' href="/home" className={classes.item}>
                        <ListItemIcon>
                            <HomeIcon />
                        </ListItemIcon>
                        <ListItemText primary="Home" />
                    </ListItem>

                    <ListItem button component='a' href="/my-wallet">
                        <ListItemIcon>
                            <VendorIcon />
                        </ListItemIcon>
                        <ListItemText primary="Wallet" />
                    </ListItem>

                    <ListItem button onClick={(event)=>{this.logout()}}>
                        <ListItemIcon>
                            <LogoutIcon />
                        </ListItemIcon>
                        <ListItemText primary="Logout" />
                    </ListItem>
                </List>
                <Divider />
            </Drawer>
        );
    }
}

Navigation.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => {
    const { loggingIn } = state.authentication;
    return {
        loggingIn
    };
}

export default connect(mapStateToProps)(withStyles(styles)(Navigation));