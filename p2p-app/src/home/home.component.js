import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import AppBar from '../_components/appbar';
import Nav from '../_components/nav';

import {
    Button,
    Grid,

    Typography,

    Paper,


} from '@material-ui/core';
const drawerWidth = 240;

const styles = theme => ({
    root: {
        flexGrow: 1,
        
    },
    appFrame: {
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
        width: '100%',
    },
    appBar: {
        width: `calc(100% - ${drawerWidth}px)`,
    },
    'appBar-left': {
        marginLeft: drawerWidth,
    },
    'appBar-right': {
        marginRight: drawerWidth,
    },
    drawerPaper: {
        position: 'relative',
        width: drawerWidth,
    },
    toolbar: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing(3),
    },

    contentRoot: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(2),
        padding: theme.spacing(4),
        height:'400px'
    },
    button:{
        marginTop:100
    }
});


class Home extends Component {

    render() {
        const { classes } = this.props;

        return (

            <div className={classes.root}>
                <div className={classes.appFrame}>
                    <AppBar />
                    <Nav />
                    <main className={classes.content}>
                        <div className={classes.toolbar} />
                        <Grid container spacing={10}>
                            <Grid item xs={3}>
                                <Typography>{'Dashboard'}</Typography>
                            </Grid>
                            <Grid item xs={6}>
                            </Grid>
                            <Grid item xs={3} container justify="flex-end">

                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item xs={3}>
                            </Grid>
                            <Grid item xs={6}>
                            </Grid>
                            <Grid item xs={3}>

                            </Grid>
                        </Grid>
                        <br />
                        <br />
                       
                        <Grid item xs={12}>
                            <div>
                                <Paper className={classes.contentRoot} elevation={1}>
                                    <Button variant="contained" color="primary" className={classes.button} component='a' href="/send-money">
                                        SEND MONEY
                                    </Button>
                                </Paper>
                            </div>

                        </Grid>
                    </main>
                </div>
            </div>
        );
    }
}

Home.propTypes = {
    classes: PropTypes.object.isRequired,
};


function mapStateToProps(state) {
    return state;
}


const connectedHomePage = withRouter(connect(mapStateToProps, null, null, {
    pure: false
})(withStyles(styles)(Home)));

export { connectedHomePage as Home };