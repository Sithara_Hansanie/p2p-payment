import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import AppBar from '../_components/appbar';
import Nav from '../_components/nav';

import {
    Button,
    Grid,
    TextField,
    Typography,
    Card,
    CardHeader,
    CardContent,
    CardActions,
    Divider,


} from '@material-ui/core';
const drawerWidth = 240;

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    appFrame: {
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
        width: '100%',
    },
    appBar: {
        width: `calc(100% - ${drawerWidth}px)`,
    },
    'appBar-left': {
        marginLeft: drawerWidth,
    },
    'appBar-right': {
        marginRight: drawerWidth,
    },
    drawerPaper: {
        position: 'relative',
        width: drawerWidth,
    },
    toolbar: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing(3),
    },
    textField: {
        marginTop: theme.spacing(2)
    },
    contentBody: {
        flexGrow: 1,
        display: 'flex',
        alignItems: 'center',
        [theme.breakpoints.down('md')]: {
            justifyContent: 'center'
        }
    },
    form: {
      alignItems:'center',
    },
});


class Form extends Component {

    render() {
        const { classes } = this.props;

        return (

            <div className={classes.root}>
                <div className={classes.appFrame}>
                    <AppBar />
                    <Nav />
                    <main className={classes.content}>
                        <div className={classes.toolbar} />
                        <Grid container spacing={10}>
                            <Grid item xs={3}>
                                <Typography>{'Transffer'}</Typography>
                            </Grid>
                            <Grid item xs={6}>
                            </Grid>
                            <Grid item xs={3} container justify="flex-end">

                            </Grid>
                        </Grid>
                      
                        <br />
                        <br />
                        <Grid container spacing={2}  className={classes.contentBody}>
                         
                                <Card >
                                    <form 
                                        autoComplete="off"
                                        noValidate
                                    >
                                        <CardHeader
                                            subheader="The information can be edited"
                                            title="SEND MONEY"
                                        />
                                        <Divider />
                                        <CardContent >
                                            
                                                <Grid 
                                                    item
                                                    lg={6}
                                                    xs={12}
                                                >
                                                    <div className={classes.form}>
                                                    <TextField
                                                    
                                                        fullWidth
                                                        helperText="Please specify the receiver name"
                                                        label="Receiver Name"
                                                        margin="dense"
                                                        name="receiver_name"
                                                        // onChange={handleChange}
                                                        required
                                                        // value={values.firstName}
                                                        variant="outlined"
                                                        className={classes.textField}

                                                    />

                                                    <TextField
                                                        fullWidth
                                                        helperText="Please specify the receiver name"
                                                        label="Receiver Email"
                                                        margin="dense"
                                                        name="receiver_email"
                                                        // onChange={handleChange}
                                                        required
                                                        // value={values.firstName}
                                                        variant="outlined"
                                                        className={classes.textField}

                                                    />
                                                     <TextField
                                                        fullWidth
                                                        helperText="Please specify the  amount "
                                                        label="Amount"
                                                        margin="dense"
                                                        name="amount"
                                                        // onChange={handleChange}
                                                        required
                                                        // value={values.firstName}
                                                        variant="outlined"
                                                        className={classes.textField}

                                                    />
                                                    </div>
                                                </Grid>

                                            
                                        </CardContent>
                                        <Divider />
                                        <CardActions>
                                            <Button
                                                color="primary"
                                                variant="contained"
                                            >
                                                SEND
                                            </Button>
                                        </CardActions>
                                    </form>
                                </Card>
                      
                        </Grid>
                    </main>
                </div>
            </div>
        );
    }
}

Form.propTypes = {
    classes: PropTypes.object.isRequired,
};


function mapStateToProps(state) {
    return state;
}


const connectedFormPage = withRouter(connect(mapStateToProps, null, null, {
    pure: false
})(withStyles(styles)(Form)));

export { connectedFormPage as Form };